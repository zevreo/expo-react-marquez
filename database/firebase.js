import firebase from "firebase";
import "firebase/firestore";

var firebaseConfig = {
    apiKey: "AIzaSyD4Gtqg7xV6ul6xcWYZC0RGjJBblwO0X5o",
    authDomain: "sm41-23bed.firebaseapp.com",
    databaseURL: "https://sm41-23bed.firebaseio.com",
    projectId: "sm41-23bed",
    storageBucket: "sm41-23bed.appspot.com",
    messagingSenderId: "567079543651",
    appId: "1:567079543651:web:6421a031d3e3496651a956"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  const db = firebase.firestore();

  export default {
      firebase, db
  };